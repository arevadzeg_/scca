import ContentWrapper from '@scandipwa/scandipwa/src/component/ContentWrapper';
import  { Checkout as SourceCheckout } from 'SourceRoute/Checkout/Checkout.component'
import './ProgressBar.style'

class Checkout extends SourceCheckout {
    



    renderStepper(){
        let stepCompleted = true
        return (
            <div className='progressBarWrapper'>
                {
                    Object.keys(this.stepMap).map((item, i) => {
                        return <>
                                <div className={`progressBar  ${!stepCompleted? "progressBar-completed": ""}`}></div>
                                        <div className={'step ' + 
                                        `${ stepCompleted && "step-completed "}`
                                        }>
                                            {   <>
                                                    <div className='step-number'>{++i}
                                                    </div>
                                                    <p>{this.stepMap[item].title}</p>
                                                </>
                                            }
                                </div>
                                {Object.keys(this.stepMap).length === i && <div className={`progressBar  ${!stepCompleted? "progressBar-completed": ""}`}></div>}
                                {
                                    item === this.props.checkoutStep ? stepCompleted = false :null
                                }
                            </>
                    })
                }
            </div>
        );
    }

    render() {
        return (
            <main block="Checkout">
                { this.renderStepper() }

                <ContentWrapper
                  wrapperMix={ { block: 'Checkout', elem: 'Wrapper' } }
                  label={ __('Checkout page') }
                >
                    { this.renderSummary(true) }
                    <div block="Checkout" elem="Step">
                        { this.renderTitle() }
                        { this.renderGuestForm() }
                        { this.renderStep() }
                        { this.renderLoader() }
                    </div>
                    <div>
                        { this.renderSummary() }
                        { this.renderPromo() }
                        { this.renderCoupon() }
                    </div>
                </ContentWrapper>
            </main>
        );
    }

}

export default Checkout